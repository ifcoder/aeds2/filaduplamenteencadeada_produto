#include <cstdlib>

#include "Produto.h"
#include "FilaDuplamenteEncadeada.h"

using namespace std;

void menuOpcoes() {
    cout << "\n\t\t-------------------------------" << endl;
    cout << "\t\t             Menu               " << endl;
    cout << "\t\t1 - Inserir(1) " << endl;
    cout << "\t\t2 - Remover(ultimo) " << endl;
    cout << "\t\tS - Sair " << endl;
    cout << "\t\t-------------------------------  " << endl;
    cout << "\t\tEscolhar uma opcao:";
}

int main() {
    int pos;
    char opcao = 's';
    FilaDuplamenteEncadeada l;

    do {
        menuOpcoes();
        cin >> opcao;
        cout << "\t\t----------------------------\n";

        switch (opcao) {
            case '1':
                l.insert();
                break;
            case '2':
                l.remove();
                break;            
        }
        l.imprimir();
    } while (opcao != 's' && opcao!='S');

    return 0;
}

