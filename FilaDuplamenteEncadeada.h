#ifndef DUPLAMENTEENCADEADA_H
#define	DUPLAMENTEENCADEADA_H

#include "Produto.h"
#include "Nodo.h"

class FilaDuplamenteEncadeada {
private:
    int quant;
    Nodo *head;

    bool isEmpty (); 
    Nodo* getElemento(int posicao);
    
    
public:
    FilaDuplamenteEncadeada();
    virtual ~FilaDuplamenteEncadeada();
    
    void insert();         
    void remove();                 
        
    void imprimir();
    void preencher();                
    
    //GETTERS E SETTERS
    void setQuant(int quant);
    int getQuant() const;
};

#endif	

