#include "FilaDuplamenteEncadeada.h"
#include<iostream>

using namespace std;

FilaDuplamenteEncadeada::FilaDuplamenteEncadeada() {
    quant = 0;
    head = NULL;
}

FilaDuplamenteEncadeada::~FilaDuplamenteEncadeada() {
    //será chamado quando o objeto for destruido
}

/**
 * Retorna o endereco do elemento da posiçao: n
 * @param posicao
 * @return 
 */
Nodo* FilaDuplamenteEncadeada::getElemento(int n) {
    Nodo* p = head;
    int j = 1;

    while (j <= n - 1 && p->getProx() != NULL) {
        p = p->getProx();
        j++;
    }

    if (j == n)
        return p;
    else
        return NULL; // posicao invalida

}

void FilaDuplamenteEncadeada::preencher() {
    int q = 0;
    do{
        cout << "Quantos elementos voce deseja inserir:";
        cin >> q;
    }while(q < 0);
    
    for(int i=0; i<= q-1 ;i++){
        this->insert();
    }
}

void FilaDuplamenteEncadeada::insert() {
    Produto p;
    p.preencher();
    Nodo *novo;
    novo = new Nodo(p);
    
    if(quant == 0){ //Fila está vazia
        head = novo;
    }else{
        Nodo* ultimo = getElemento(quant);
        ultimo->setProx(novo);
        novo->setAnt(ultimo);
    }
    
    quant++;
}


void FilaDuplamenteEncadeada::remove() {
    if(quant > 0){
        if(quant == 1){
            head = head->getProx();
            //não tem segundo elemento para fazer o apontamento para trás
        }else { //lista tem mais de um elemento
            head = head->getProx();
            head->setAnt(NULL);
        }
        quant--;
    }else{
        cout << "Operacao invalida - Lista vazia";
    }
}

bool FilaDuplamenteEncadeada::isEmpty() {
    if (quant == 0)
        return true;
    else
        return false;

}

void FilaDuplamenteEncadeada::imprimir() {
    cout << "\n>> Lista [ ";
    if (!isEmpty()) {
        Nodo* p = head;
        while (p != NULL) {
            p->getItem().imprimirResumido();
            p = p->getProx();
        }
    }
    cout << " ] \n";
}

/**
 *  GETTERS E SETTERS 
 */
void FilaDuplamenteEncadeada::setQuant(int quant) {
    this->quant = quant;
}

int FilaDuplamenteEncadeada::getQuant() const {
    return quant;
}
